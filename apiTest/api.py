from fastapi import FastAPI
from fastapi import Request
from models import *
from cryptData import Crypt
crypt = Crypt()
crypt.createNewKeys()

app = FastAPI()

@app.get("/")
async def root():
    return {"Hello": "World"}

@app.get("/v1/public_key")
async def fetch_public_key():
    """Get the public rsa key for securely sending sensitive data"""
    return {"state": "ok", "response": {"public_key": [crypt.getPublicKey().n, crypt.getPublicKey().e]}}

@app.post("/v1/user")
async def register_user(user: UserObject):
    return {"state": "ok", "response": {"id": user.id}}

@app.get("/v1/user")
async def fetch_user(id: UUID):
    return {"state": "ok"}

@app.get("/v1/stats")
async def fetch_stats(user: UserLoginObject):
    return {"state": "ok"}

@app.post("/v1/stats")
async def create_tank_stat(user: UserLoginObject, tank: TankObject):
    return {"state": "ok"}

@app.post("/v1/test")
async def test_crypt(data: Request):
    data:bytes = await data.body()
    decData = crypt.decryptData(data)
    return {"state": "ok", "response": {"decData": decData}}

