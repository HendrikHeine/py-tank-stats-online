import rsa

class Crypt:
    def __init__(self, nbits = 512, privateKey = None, publicKey = None) -> None:
        self.nbits = nbits
        self.__privateKey = privateKey
        self.__publicKey = publicKey

    def createNewKeys(self):
        self.__publicKey, self.__privateKey = rsa.newkeys(self.nbits)

    def getPublicKey(self):
        return self.__publicKey

    def encryptData(self, data:str):
        return rsa.encrypt(data.encode(), self.__publicKey)

    def decryptData(self, data:bytes):
        return rsa.decrypt(data, self.__privateKey).decode()

    def setPrivateKey(self, n:int, e:int, d:int, p:int, q:int):
        self.__privateKey = rsa.PrivateKey(n, e, d, p, q)

    def setPublicKey(self, n:int, e:int):
        self.__publicKey = rsa.PublicKey(n, e)