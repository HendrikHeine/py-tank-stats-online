from pydantic import BaseModel
from typing import Optional
from uuid import UUID
from uuid import uuid4

class UserObject(BaseModel):
    id: Optional[UUID] = uuid4()
    email: str
    password: bytes

class UserLoginObject(BaseModel):
    email: str
    password: bytes

class TankObject(BaseModel):
    id: Optional[UUID] = uuid4()
    price_per_liter: float
    liter: float
    km: float
    date: int
    user: UUID

class EncryptedData(BaseModel):
    data: str
