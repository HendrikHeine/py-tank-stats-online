import requests
import json

class API:
    def __init__(self, url:str) -> None:
        self.__url = url
    
    def getInfo(self):
        response = requests.api.get(url=f"{self.__url}/")
        if response.status_code == 200:
            return json.loads(response.text)
        else:
            return None

    def getPublicKey(self):
        response = requests.api.get(url=f"{self.__url}/v1/public_key")
        if response.status_code == 200:
            return json.loads(response.text)
        else:
            return None

    def sendData(self, data):
        response = requests.api.post(url=f"{self.__url}/v1/test", data=data)
        if response.status_code == 200:
            return json.loads(response.text)
        else:
            return None