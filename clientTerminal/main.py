import apiHandler
import cryptData


api = apiHandler.API(url="http://localhost:9999")
crypt = cryptData.Crypt()

key = api.getPublicKey()
key_n = key["response"]["public_key"][0]
key_e = key["response"]["public_key"][1]

crypt.setPublicKey(n=key_n, e=key_e)
data = crypt.encryptData(data="Hallo Du daaa")
response = api.sendData(data=data)
print(response)