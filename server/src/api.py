from fastapi import FastAPI
description = """
/v1/doc for SwaggerUI

/v1/redoc for ReDoc
"""


app = FastAPI(docs_url="/v1/doc", redoc_url="/v1/redoc", title="TankStats", version="1.0.1", description=description )

@app.get("/v1")
async def root():
    """Get some informations about the API"""
    return {'state': 'ok', 'info': {'version': app.version, 'title': app.title}}

@app.post("/v1/create_new_user")
async def create_new_user():
    """Create new user"""
    return {'state': 'ok'}
