import psycopg2

class Database:
    def __init__(self) -> None:
        self.exception = None
        self.host = "localhost"
        self.port = 5432
        self.database = "TankStats"
        self.__POSTGRESQL_USER = "postgres"
        self.__POSTGRESQL_PWD = "ft9XZkqfM4NvVUTfVFp32NtQxoTkXeJq6vLzdwPAo"
        self.__connectToDatabase()

    def __connectToDatabase(self):
        try:
            self.__connection = psycopg2.connect(host=self.host, port=self.port, dbname=self.database, user=self.__POSTGRESQL_USER, password=self.__POSTGRESQL_PWD)
        except Exception as err:
            self.exception = err

    def close(self):
        self.__connection.close()