\connect "TankStats";

CREATE SEQUENCE stats_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."stats" (
    "id" integer DEFAULT nextval('stats_id_seq') NOT NULL,
    "price_per_liter" numeric NOT NULL,
    "liter" numeric NOT NULL,
    "km" numeric NOT NULL,
    "date" timestamp NOT NULL,
    "user" integer NOT NULL,
    CONSTRAINT "stats_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


CREATE SEQUENCE user_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."user" (
    "id" integer DEFAULT nextval('user_id_seq') NOT NULL,
    "email" text NOT NULL,
    "password_hash" integer NOT NULL,
    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
) WITH (oids = false);
