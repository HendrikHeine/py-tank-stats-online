import bcrypt

password = b"SecretPassword55"
print(bcrypt.gensalt())
hashed = bcrypt.hashpw(password, bcrypt.gensalt())

if bcrypt.checkpw(password, hashed):
    print("Yes")
else:
    print("No")
