import uvicorn
import api

uvicorn.run(app=api.app, host="0.0.0.0", port=9999, debug=True)
